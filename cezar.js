var ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");
var alphabetLength = ALPHABET.length;

$( document ).ready(function() {
    var min = 0;
    var max = alphabetLength;
    select = document.getElementById('ceasar-select');

    for (var i = min; i<=max; i++){
        var opt = document.createElement('option');
        opt.value = i;
        opt.innerHTML = i;
        select.appendChild(opt);
    }
    $("#ceasar-button").on("click", function(){
        var encrypted = "";
        var cryptingKey = parseInt($('#ceasar-select').children('option:selected').val());
        var password = $("#ceasar-password").val().toUpperCase();
        for (var i=0; i<password.length;i++){
            new_index = (ALPHABET.indexOf(password[i]) + cryptingKey) % alphabetLength;
            password[i] = ALPHABET[new_index];
            encrypted = encrypted + ALPHABET[new_index];
        }
        $("#cryptographed-ceasar").text(encrypted);
    });

});