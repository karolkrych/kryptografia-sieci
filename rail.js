
  function encrypt( clearText, key) {
    result = "";
	increment = 1;
	row = 0;
	col = 0;
	
    matrix = [];
    for (var i=0; i<key; i++){ // tworzenie tablicy tablic z pustymi wartościami
        matrix.push([]);
        for (var j=0; j<clearText.length; j++){
            matrix[i].push("");
        }
    }

	clearText.split("").forEach(c => { // uzupełnianie tablic wartością
        if (row + increment < 0 || row + increment >= matrix.length){ // jeśli natrafimy na krawędź, musimy odbić w drugą stronę
            increment = increment * -1
        }
			
		matrix[row][col] = c;

		row += increment;
		col += 1;
    });
		
	matrix.forEach(list => {
        list.forEach(c => {
            result += c;
        });
        
    });

	return result
  }


$( document ).ready(function() {;
    select = document.getElementById('rail-select');

    for (var i = 2; i<=10; i++){ // tworzenie opcji do selecta
        var opt = document.createElement('option');
        opt.value = i;
        opt.innerHTML = i;
        select.appendChild(opt);
    }

    $("#rail-button").on("click", function(){
        var encrypted = "";
        var password = $("#rail-password").val();
        var cryptingKey = parseInt($('#rail-select').children('option:selected').val());
        console.log(cryptingKey);
        encrypted = encrypt(password, cryptingKey);
        $("#cryptographed-rail").text(encrypted);
    });

});