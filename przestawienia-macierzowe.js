$( document ).ready(function() {
    $("#matrix-button").on("click", function(){
        var password = $("#matrix-password").val();
        rows = chunk(password, 5);
        encrypted = "";
        rows.forEach(row => {
            [2, 3, 0, 4, 1].forEach(index => {
                if (row.length > index)
                    encrypted += row[index];    
            });
            
        });
        $("#cryptographed-matrix").text(encrypted);
    });


    function chunk(str, size) {
        return str.match(new RegExp('.{1,' + size + '}', 'g'));
    }
});