var parseToInts = function(text) {
    return text.split(' ').map(function(item) {
        return parseInt(item, 10);
    });
}

$( document ).ready(function() {
    $("#lfsr-button").on("click", function(){
        var key = parseToInts($("#lfsr-key").val());
        var elementsToXor = parseToInts($("#lfsr-level").val());
        var newElement = key[elementsToXor[0] - 1];
        elementsToXor.shift();
        elementsToXor.forEach(elem => {
            newElement = newElement ^ key[elem - 1];
        });
        key.pop();
        key.unshift(newElement);
        $("#decimal-lfsr").text(key.join(" "));
    });
});