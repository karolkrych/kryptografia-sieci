var ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");
var alphabetLength = ALPHABET.length;

$( document ).ready(function() {
    $("#vigener-button").on("click", function(){
        var encrypted = "";
        var password = $("#vigener-password").val().toUpperCase();
        var vigenerKey = $("#vigener-key").val().toUpperCase();
        for (var i=0; i<password.length;i++){
            while (password.length > vigenerKey.length){
                vigenerKey += vigenerKey;
            }
            var cryptingKey = parseInt(ALPHABET.indexOf(vigenerKey[i]));
            new_index = (ALPHABET.indexOf(password[i]) + cryptingKey) % alphabetLength;
            password[i] = ALPHABET[new_index];
            encrypted = encrypted + ALPHABET[new_index];
        }
        $("#cryptographed-vigener").text(encrypted);
    });

});